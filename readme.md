Minimal Project :
=================

Doc : [Notion](https://rafi-riday.notion.site/CSE115-Project-1-minimal-5fc1e0000f1e442baaff354506b261df)

-   **Admin**
    -   **username :** *admin*
    -   **password :** *pass*

-   **Teacher**
    *Not implemented yet :[*

-   **Student**
    *Not implemented yet :[*


Old project :
=============

Updated : https://www.notion.so/rafi-riday/CSE115-Project-1-0e3420cc05804edfaf26a054ce3fb841

CSE115 Project 1
================

TEAM CLOSER'S
=============

PROJECT NAME - RDS 4.0
----------------------

Project overview :
------------------

- Project planning
    - Flowchart
    ```mermaid
        flowchart LR
            example1>"UI / Window"] ~~~ example2[/"Button press"/] ~~~ example3["Process"]
            example4(["End"]) ~~~ example5{"Decision"} ~~~ example6[("Database")]
    ```
    ```mermaid
        flowchart TD
            login>"Login UI"] -->|"Login as admin"| admin>"Admin UI"]
            login -->|"Login as student/teacher"| not-implemented(["Not implemented yet"])
            admin --> add-btn[/"Add new student"/]
            add-btn --> info-1>"Take Id"]
            info-1 --> check-id-2["Id exists?"]
            admin --> edit-btn[/"Edit student"/]
            edit-btn --> info-2>"Take Id"]
            admin ----> search-btn[/"Search student"/]
            search-btn --> search-ui>"Search UI"]
            search-ui --> show-student>"Show student"]
            check-id-3["Id exists?\n"] -->|"No"| prompt-2{"Add new data?"}
            check-id-2 -->|"No"| info-4>"Take other info"]
            prompt-2 -->|"Yes"| info-4
            confirm-delete{"Are you sure to delete Id?"} -->|"Yes"| delete1[("Delete Id")]
            admin --> delete-btn[/"Delete student"/]
            delete-btn --> info-3>"Take Id"]
            info-2 --> check-id-3
            info-4 -->|"Step 2"| show-student
            confirm-delete -->|"No"| info-3
            info-3 --> check-id-1["Id exists?"]
            check-id-1 -->|"Yes"| confirm-delete
            check-id-1 -->|"No"| error>"Error"]
            prompt-1{"Change old data?"} -->|"Yes"| delete2["delete_id = true"]
            delete2 --> info-4
            check-id-3 -->|"Yes"| delete2
            info-4 -->|"Step 1"| 691487[("delete_id ? delete : continue")]
            691487 --> db1[("Add new data")]
            prompt-2 -->|"No"| info-2
            prompt-1 -->|"No"| info-1
            check-id-2 -->|"Yes"| prompt-1

        %% style
            style login fill:#cc1b44
            style admin fill:#1f9150
            style not-implemented fill:#406060

            linkStyle 2 stroke:#0000ff
            linkStyle 3 stroke:#0000ff,fill:none
            linkStyle 4 stroke:#0000ff,fill:none
            linkStyle 5 stroke:#03ff11
            linkStyle 6 stroke:#00ff0c
            linkStyle 10 stroke:#00ff22
            linkStyle 11 stroke:#0000ff,fill:none
            linkStyle 12 stroke:#05ff00
            linkStyle 13 stroke:#ff0000,fill:none
            linkStyle 14 stroke:#ff0000
            linkStyle 15 stroke:#ff0000,fill:none
            linkStyle 16 stroke:#00ff3c
            linkStyle 18 stroke:#ff0000,fill:none
            linkStyle 19 stroke:#ff0000,fill:none
            linkStyle 20 stroke:#ff0000,fill:none
            linkStyle 21 stroke:#ff0000,fill:none
            linkStyle 22 stroke:#0000ff,fill:none
            linkStyle 24 stroke:#00ff09
            linkStyle 27 stroke:#0dff35
            linkStyle 28 stroke:#0000ff,fill:none
            linkStyle 29 stroke:#0000ff,fill:none
    ```
    - Flowchart with more optimization
        ![Flowchart Mini](img/flow-mini.png)

-   **Student Module :**
    Students can log in and access the following features :
    -   Enroll in Courses
    -   View Grade History
    -   View Attendance History
    -   Check Their Profile

-   **Teacher Module :**
    Teachers can log in and access the following features :
    -   Take Attendance
    -   Check Their Profile
    -   Check Their Salary

-   **Database :**
    -   Student Information (ID, Name, Courses Enrolled, Grades, Attendance)
    -   Teacher Information (ID, Name, Courses Taught, Salary)
    -   Course Information (Course ID, Course Name, Time, Date, etc.)

-   **Login System :**
    Simple student/teacher id and password login system.

-   **Enroll in Courses :**
    Students can enroll in available courses. Update the student's course list and the course's student list.

-   **View Grade History :**
    Students can view their grade history for each course they are enrolled in.

-   **View Attendance History :**
    Students can view their attendance history for each course they are enrolled in.

-   **Check Profile :**
    Students and teachers can check their profile information, including their name, ID, and other details.

-   **Take Attendance :**
    Teachers can take attendance for their courses, marking students as present or absent for each class. Update the attendance history.

-   **Calculate Salary :**
    Calculate the teacher's salary based on the number of courses/classes they are teaching.

**Coding ethics :**
-------------------
-   Use **snake_case** naming convention
-   Use different function for visualization (no params/returns) and operation (with params/returns)
-   **Static data** will be saved in text files (**array like** data structure), **dynamic data** (**user inputs**) will be saved in binary format
-   Using new lines to seperate different part (Readibility)
-   Different files for different modules, a main.c file to combine them
-   DO NOT TRY TO MAKE CONSOLE TEXT CENTERED (left aligned printed text is important)
