// public variables
#define GRID_ROW_SPACING 5
#define GRID_COL_SPACING 5

GtkWidget *stack, *window;
int dialog_selected;

const char *dialog_buttons_1[] = {"Ok", NULL};
const char *dialog_buttons_2[] = {"Continue?", "Cancel", NULL};

enum
{
    LOGIN_AS_SELECT,
    LOGIN_AS_ADMIN,
    LOGIN_AS_TEACHER,
    LOGIN_AS_STUDENT,
} login_as;

enum ADMIN_ROUT
{
    ADMIN_ROUT_ADD = 1,
    ADMIN_ROUT_EDIT,
    ADMIN_ROUT_DELETE,
    ADMIN_ROUT_SEARCH,
} admin_route;

struct STUDENT_DATA
{
    char name[128];
    char id[16];
    char passwd[64];
} student_data;

// change stack button
void on_click_change_stack_btn(GtkWidget *btn, gpointer stack_name)
{
    gtk_stack_set_visible_child_name(GTK_STACK(stack), stack_name);
}

GtkWidget *f_change_stack_btn(char *str, char *stack_name)
{

    GtkWidget *btn = gtk_button_new_with_label(str);
    g_signal_connect(btn, "clicked", G_CALLBACK(on_click_change_stack_btn), stack_name);

    return btn;
}

// Header
GtkWidget *f_header(char *msg)
{
    char str[256];
    sprintf(str, "<big><big>%s</big></big>", msg);

    GtkWidget *header = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(header), str);

    return header;
}

// error page
GtkWidget *f_new_error_page(char *back_stack_name)
{
    GtkWidget *page = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    gtk_box_append(GTK_BOX(page), f_header("An error occurred\0"));
    gtk_box_append(GTK_BOX(page), f_change_stack_btn("Back", back_stack_name));

    return page;
}

// dialog
void f_on_choose_dialog(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
    GtkAlertDialog *dialog = GTK_ALERT_DIALOG(source_object);
    GError *err = NULL;

    int button = gtk_alert_dialog_choose_finish(dialog, res, &err);

    if (err)
    {
        gtk_stack_set_visible_child_name(GTK_STACK(stack), "admin-take-id-error-page");

        return;
    }

    dialog_selected = button;
    // g_assert_not_reached();
}

GtkAlertDialog *f_dialog_new(char *msg, char *details, const char **buttons, int default_btn, int cancel_btn)
{
    GtkAlertDialog *dialog = gtk_alert_dialog_new("Alert");

    gtk_alert_dialog_set_message(dialog, msg);
    gtk_alert_dialog_set_detail(dialog, details);

    gtk_alert_dialog_set_buttons(dialog, buttons);
    gtk_alert_dialog_set_default_button(dialog, default_btn);
    if (default_btn != cancel_btn)
        gtk_alert_dialog_set_cancel_button(dialog, cancel_btn);

    return dialog;
}
