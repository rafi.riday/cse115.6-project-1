// public variables
GtkWidget *dropdown, *login_username_input, *login_password_input, *login_hidable_grid_1;

void login_page_f_on_dropdown_changed(GtkDropDown *dropdown, gpointer user_data)
{
    login_as = gtk_drop_down_get_selected(dropdown);

    if (login_as != LOGIN_AS_SELECT)
        gtk_widget_set_visible(login_hidable_grid_1, TRUE);
    else
        gtk_widget_set_visible(login_hidable_grid_1, FALSE);
}

void login_page_f_on_click_reset(GtkWidget *btn, gpointer data)
{
    gtk_drop_down_set_selected(GTK_DROP_DOWN(dropdown), 0);
    gtk_editable_set_text(GTK_EDITABLE(login_username_input), "");
    gtk_editable_set_text(GTK_EDITABLE(login_password_input), "");
}

void login_page_f_on_click_login(GtkWidget *btn, gpointer data)
{
    if (login_as == LOGIN_AS_ADMIN)
    {
        if (strcmp(gtk_editable_get_text(GTK_EDITABLE(login_username_input)), "admin") == 0 && strcmp(gtk_editable_get_text(GTK_EDITABLE(login_password_input)), "pass") == 0)
        {
            gtk_stack_set_visible_child_name(GTK_STACK(stack), "admin-launch-page");
        }
    }

    // TODO: to be implemented
    else if (login_as == LOGIN_AS_TEACHER)
        gtk_stack_set_visible_child_name(GTK_STACK(stack), "teacher-launch-page");

    else if (login_as == LOGIN_AS_STUDENT)
        gtk_stack_set_visible_child_name(GTK_STACK(stack), "student-launch-page");
}

GtkWidget *f_login_page()
{
    GtkWidget *page, *grid, *reset, *login;
    GtkStringList *list;

    // dropdown
    list = gtk_string_list_new(NULL);

    gtk_string_list_append(list, "Select");
    gtk_string_list_append(list, "Admin");
    gtk_string_list_append(list, "Teacher");
    gtk_string_list_append(list, "Student");

    dropdown = gtk_drop_down_new(G_LIST_MODEL(list), NULL);
    g_signal_connect(dropdown, "notify::selected", G_CALLBACK(login_page_f_on_dropdown_changed), NULL);

    // input
    login_username_input = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(login_username_input), "username");

    login_password_input = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(login_password_input), "password");

    reset = gtk_button_new_with_label("Reset");
    g_signal_connect(reset, "clicked", G_CALLBACK(login_page_f_on_click_reset), NULL);
    login = gtk_button_new_with_label("Login");
    g_signal_connect(login, "clicked", G_CALLBACK(login_page_f_on_click_login), NULL);

    // placement
    grid = gtk_grid_new();
    login_hidable_grid_1 = gtk_grid_new();

    gtk_grid_attach(GTK_GRID(login_hidable_grid_1), gtk_label_new("Username"), 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(login_hidable_grid_1), login_username_input, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(login_hidable_grid_1), gtk_label_new("Password"), 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(login_hidable_grid_1), login_password_input, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(login_hidable_grid_1), reset, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(login_hidable_grid_1), login, 1, 2, 1, 1);

    gtk_grid_attach(GTK_GRID(grid), f_header("Login"), 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid), gtk_label_new("Login as"), 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), dropdown, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), login_hidable_grid_1, 0, 2, 2, 3);

    page = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    gtk_box_append(GTK_BOX(page), grid);

    // styles **************************************************
    // grid
    gtk_grid_set_row_spacing(GTK_GRID(grid), GRID_ROW_SPACING);
    gtk_grid_set_column_spacing(GTK_GRID(grid), GRID_COL_SPACING);
    gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);

    // login_hidable_grid_1
    gtk_grid_set_row_spacing(GTK_GRID(login_hidable_grid_1), GRID_ROW_SPACING);
    gtk_grid_set_column_spacing(GTK_GRID(login_hidable_grid_1), GRID_COL_SPACING);
    gtk_grid_set_column_homogeneous(GTK_GRID(login_hidable_grid_1), TRUE);

    // page
    gtk_widget_set_halign(page, GTK_ALIGN_CENTER);
    gtk_widget_set_valign(page, GTK_ALIGN_CENTER);

    // visibility
    gtk_widget_set_visible(login_hidable_grid_1, FALSE);
    // styles **************************************************

    return page;
}
