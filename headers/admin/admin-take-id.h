GtkWidget *admin_take_id_input;

void take_id_on_submit()
{
    char entered_string_len = strlen(gtk_editable_get_text(GTK_EDITABLE(admin_take_id_input)));

    if (entered_string_len < 7 || entered_string_len > 15)
    {
        gtk_alert_dialog_choose(f_dialog_new("Invalid Id length", "Please enter at least 7 and max 15 characters", dialog_buttons_1, 0, 0), GTK_WINDOW(window), NULL, f_on_choose_dialog, NULL);
        return;
    }
    // const char *buttons[] = {"Continue?", "Cancel", NULL};
    // gtk_alert_dialog_choose(f_dialog_new("Message", "More details", buttons, 0, 1), GTK_WINDOW(window), NULL, f_on_choose_dialog, NULL);

    switch (admin_route)
    {
    case ADMIN_ROUT_ADD:

        break;

    case ADMIN_ROUT_EDIT:
        gtk_alert_dialog_choose(f_dialog_new("EDIT command", "Ok to proceed", dialog_buttons_1, 0, 0), GTK_WINDOW(window), NULL, f_on_choose_dialog, NULL);
        break;

    case ADMIN_ROUT_SEARCH:
        gtk_alert_dialog_choose(f_dialog_new("SEARCH command", "Ok to proceed", dialog_buttons_1, 0, 0), GTK_WINDOW(window), NULL, f_on_choose_dialog, NULL);
        break;

    case ADMIN_ROUT_DELETE:
        gtk_alert_dialog_choose(f_dialog_new("DELETE command", "Ok to proceed", dialog_buttons_1, 0, 0), GTK_WINDOW(window), NULL, f_on_choose_dialog, NULL);
        break;

    default:
        gtk_alert_dialog_choose(f_dialog_new("Invalid rout", "Not Add/Edit/Delete/Search command", dialog_buttons_1, 0, 0), GTK_WINDOW(window), NULL, f_on_choose_dialog, NULL);
        break;
    }
}

GtkWidget *f_admin_take_id()
{
    GtkWidget *page, *submit_btn;

    // id input
    admin_take_id_input = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(admin_take_id_input), "max 15 characters");
    submit_btn = gtk_button_new_with_label("Submit");
    g_signal_connect(submit_btn, "clicked", G_CALLBACK(take_id_on_submit), NULL);

    page = gtk_grid_new();

    gtk_grid_attach(GTK_GRID(page), f_header("Enter Id"), 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(page), admin_take_id_input, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(page), submit_btn, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(page), f_change_stack_btn("Back", "admin-launch-page"), 0, 2, 2, 1);

    // style
    gtk_grid_set_row_spacing(GTK_GRID(page), GRID_ROW_SPACING);
    gtk_grid_set_column_spacing(GTK_GRID(page), GRID_COL_SPACING);
    gtk_grid_set_column_homogeneous(GTK_GRID(page), TRUE);

    return page;
}
