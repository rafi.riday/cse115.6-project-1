void admin_f_on_click_admin_btn(GtkWidget *btn, enum ADMIN_ROUT rout_id)
{
    admin_route = rout_id;
    gtk_stack_set_visible_child_name(GTK_STACK(stack), "admin-take-id");
}

GtkWidget *f_admin_launch_page()
{
    GtkWidget *page, *grid, *btn_add, *btn_edit, *btn_delete, *btn_search;

    grid = gtk_grid_new();

    btn_add = gtk_button_new_with_label("Add");
    g_signal_connect(btn_add, "clicked", G_CALLBACK(admin_f_on_click_admin_btn), (enum ADMIN_ROUT *)ADMIN_ROUT_ADD);
    btn_edit = gtk_button_new_with_label("Edit");
    g_signal_connect(btn_edit, "clicked", G_CALLBACK(admin_f_on_click_admin_btn), (enum ADMIN_ROUT *)ADMIN_ROUT_EDIT);
    btn_delete = gtk_button_new_with_label("Delete");
    g_signal_connect(btn_delete, "clicked", G_CALLBACK(admin_f_on_click_admin_btn), (enum ADMIN_ROUT *)ADMIN_ROUT_DELETE);
    btn_search = gtk_button_new_with_label("Search");
    g_signal_connect(btn_search, "clicked", G_CALLBACK(admin_f_on_click_admin_btn), (enum ADMIN_ROUT *)ADMIN_ROUT_SEARCH);

    // placement
    gtk_grid_attach(GTK_GRID(grid), f_header("Admin UI"), 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid), btn_add, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), btn_edit, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), btn_delete, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), btn_search, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), f_change_stack_btn("Back", "login-page"), 0, 3, 2, 1);

    page = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);

    gtk_box_append(GTK_BOX(page), grid);

    // style
    gtk_grid_set_row_spacing(GTK_GRID(grid), GRID_ROW_SPACING);
    gtk_grid_set_column_spacing(GTK_GRID(grid), GRID_COL_SPACING);
    gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);

    return page;
}
