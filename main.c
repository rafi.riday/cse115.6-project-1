#include <gtk/gtk.h>
#include <string.h>

// headers
#include "headers/common.h"
#include "headers/login.h"
#include "headers/admin/admin.h"
#include "headers/admin/admin-take-id.h"
#include "headers/student.h"
#include "headers/teacher.h"

static void
activate(GtkApplication *app, gpointer user_data);

int main(int argc, char **argv)
{
    GtkApplication *app;
    int status;

    app = gtk_application_new("org.closers.project1", G_APPLICATION_DEFAULT_FLAGS);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);

    return status;
}

static void activate(GtkApplication *app, gpointer user_data)
{
    GtkWidget *scroll_window, *login_page, *admin_launch_page, *admin_take_id, *student_launch_page, *teacher_launch_page, *login_error_page, *admin_take_id_error_page;

    stack = gtk_stack_new();
    gtk_widget_set_halign(stack, GTK_ALIGN_CENTER);
    gtk_widget_set_valign(stack, GTK_ALIGN_START);
    gtk_widget_set_margin_top(stack, 16);
    gtk_widget_set_margin_bottom(stack, 16);

    // Create and add the pages to the stack
    login_page = f_login_page();
    admin_launch_page = f_admin_launch_page();
    admin_take_id = f_admin_take_id();
    teacher_launch_page = f_teacher_launch_page();
    student_launch_page = f_student_launch_page();
    login_error_page = f_new_error_page("login-page");
    admin_take_id_error_page = f_new_error_page("admin-take-id");

    gtk_stack_add_named(GTK_STACK(stack), login_page, "login-page");
    gtk_stack_add_named(GTK_STACK(stack), admin_launch_page, "admin-launch-page");
    gtk_stack_add_named(GTK_STACK(stack), admin_take_id, "admin-take-id");
    gtk_stack_add_named(GTK_STACK(stack), teacher_launch_page, "teacher-launch-page");
    gtk_stack_add_named(GTK_STACK(stack), student_launch_page, "student-launch-page");
    gtk_stack_add_named(GTK_STACK(stack), login_error_page, "login-error-page");
    gtk_stack_add_named(GTK_STACK(stack), admin_take_id_error_page, "admin-take-id-error-page");

    gtk_stack_set_visible_child_name(GTK_STACK(stack), "login-page");

    // window
    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "RDS 4.0");
    gtk_window_set_default_size(GTK_WINDOW(window), 450, 350);

    scroll_window = gtk_scrolled_window_new();
    gtk_scrolled_window_set_child(GTK_SCROLLED_WINDOW(scroll_window), stack);
    gtk_window_set_child(GTK_WINDOW(window), scroll_window);

    gtk_window_present(GTK_WINDOW(window));
}
