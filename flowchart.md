```mermaid
flowchart TD
	login>"Login UI"] -->|"Login as admin"| admin>"Admin UI"]
	login -->|"Login as student/teacher"| not-implemented(["Not implemented yet"])
	admin --> add-btn[/"Add new student"/]
	add-btn --> info-1>"Take Id"]
	info-1 --> check-id-2["Id exists?"]
	admin --> edit-btn[/"Edit student"/]
	edit-btn --> info-2>"Take Id"]
	admin ----> search-btn[/"Search student"/]
	search-btn --> search-ui>"Search UI"]
	search-ui --> show-student>"Show student"]
	check-id-3["Id exists?\n"] -->|"No"| prompt-2{"Add new data?"}
	check-id-2 -->|"No"| info-4>"Take other info"]
	prompt-2 -->|"Yes"| info-4
	confirm-delete{"Are you sure to delete Id?"} -->|"Yes"| delete1[("Delete Id")]
	admin --> delete-btn[/"Delete student"/]
	delete-btn --> info-3>"Take Id"]
	info-2 --> check-id-3
	info-4 -->|"Step 2"| show-student
	confirm-delete -->|"No"| info-3
	info-3 --> check-id-1["Id exists?"]
	check-id-1 -->|"Yes"| confirm-delete
	check-id-1 -->|"No"| error>"Error"]
	prompt-1{"Change old data?"} -->|"Yes"| delete2["delete_id = true"]
	delete2 --> info-4
	check-id-3 -->|"Yes"| delete2
	info-4 -->|"Step 1"| 691487[("delete_id ? delete : continue")]
	691487 --> db1[("Add new data")]
	prompt-2 -->|"No"| info-2
	prompt-1 -->|"No"| info-1
	check-id-2 -->|"Yes"| prompt-1

example1>"UI / Window"]
example2[/"Button press"/]
example3["Process"]
example4(["End"])
example5{"Decision"}
example6[("Database")]

%% style
	style login fill:#cc1b44
	style admin fill:#1f9150
	style not-implemented fill:#406060

	linkStyle 2 stroke:#0000ff
	linkStyle 3 stroke:#0000ff,fill:none
	linkStyle 4 stroke:#0000ff,fill:none
	linkStyle 5 stroke:#03ff11
	linkStyle 6 stroke:#00ff0c
	linkStyle 10 stroke:#00ff22
	linkStyle 11 stroke:#0000ff,fill:none
	linkStyle 12 stroke:#05ff00
	linkStyle 13 stroke:#ff0000,fill:none
	linkStyle 14 stroke:#ff0000
	linkStyle 15 stroke:#ff0000,fill:none
	linkStyle 16 stroke:#00ff3c
	linkStyle 18 stroke:#ff0000,fill:none
	linkStyle 19 stroke:#ff0000,fill:none
	linkStyle 20 stroke:#ff0000,fill:none
	linkStyle 21 stroke:#ff0000,fill:none
	linkStyle 22 stroke:#0000ff,fill:none
	linkStyle 24 stroke:#00ff09
	linkStyle 27 stroke:#0dff35
	linkStyle 28 stroke:#0000ff,fill:none
	linkStyle 29 stroke:#0000ff,fill:none
```